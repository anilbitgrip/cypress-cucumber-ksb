# language: en
@global-search    @website    @stable    @sprint-69    @frontend    @en-se    @alltypes-search    
Feature: Alltypes Search - type series

    Background: 
        Given the user is on site en-se
        And the user is on page GlobalSearch

    @AUTOMATED @KSBP-3565 
    Scenario: Shows 999+ in tabs when more than 999 results are found
        When the user clicks onto the GlobalProductSearchInputButton
        Then the element TabsButtonBadge-1 contains text value 999+
        And the element TabsButtonBadge-3 contains text value 999+

    @AUTOMATED @KSBP-3565 
    Scenario: Tab spare parts material numbers is disabled
        Then the element TabsButton-2 is visible
        And the element TabsButton-2 is disabled
        And the element TabsButtonBadge-2 is not visible

    @AUTOMATED @WIP @KSBP-3565 
    Scenario: Type series are initially sorted alphabetically
        When the user enters value sisto into GlobalProductSearchInput
        And the user clicks onto the GlobalProductSearchInputButton
        Then the element ProductCardHeadline contains text value SISTO-10
        And the user clicks onto the ShowMoreButton
        Then the element ProductCardHeadline contains text value SISTO-VentNA

    @AUTOMATED @KSBP-3565 
    Scenario: Search global with exactly 5 results on type series
        When the user enters value sewa into GlobalProductSearchInput
        And the user clicks onto the GlobalProductSearchInputButton
        Then the element TabsButton-0 is visible
        And the element TabsButton-0 is enabled
        And the element TabsButtonBadge-0 is visible
        And the element TabsButtonBadge-0 contains text value 5
        When the user clicks onto the TabsButton-0
        Then the element ProductSearchResultCard-3 is visible
        Then the element ProductSearchResultCard-4 is not visible
        And the element ShowMoreButton is visible

    @AUTOMATED @KSBP-3565 @KSBP-4277 
    Scenario: Search global with more than 5 results on type series
        When the user enters value sisto into GlobalProductSearchInput
        And the user clicks onto the GlobalProductSearchInputButton
        Then the element TabsButton-0 is visible
        And the element TabsButton-0 is enabled
        And the element TabsButtonBadge-0 is visible
        When the user clicks onto the TabsButton-0
        Then the element ProductSearchResultCard-3 is visible
        And the element ProductSearchResultCard-4 is not visible
        And the element ShowMoreButton is visible
        When the user clicks onto the ShowMoreButton
        Then the element ProductSearchResultCard-4 is visible
        And the element ShowMoreButton is not visible

    @AUTOMATED @KSBP-3565 
    Scenario: Search anything with no results on type series
        When the user enters value YouCannotFindMeBecauseIDontExist into GlobalProductSearchInput
          And the user clicks onto the GlobalProductSearchInputButton
        Then the content Tabs is not visible
          And the element ProductSearchResultCard-0 is not visible
          And the element ShowMoreButton is not visible
          And the element NoSearchResultsHeading is visible
          And the element Suggestions is visible
          And the element ListItem is visible
          And the element ListItemLink is visible

    @AUTOMATED @KSBP-3565 
    Scenario: Search global with 1 active Tab
        When the user enters value sisto into GlobalProductSearchInput
        And the user clicks onto the GlobalProductSearchInputButton
        Then the element TabsButton-1 is disabled


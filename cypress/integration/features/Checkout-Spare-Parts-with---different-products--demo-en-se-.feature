# language: en
@fullRegisteredUser1    @stable    @spareParts    @new    @shop    @en-se    @website    @checkout    
Feature: Checkout Spare Parts with 2 different products (en-se)

    Complete checkout flow with one default product.
      Starts at empty cart, ads products and proceeds until the confirmation page.

    Background: logged in as fullRegisteredUser1 on site en-se on page ShoppingCart
        Given the user is on site en-se
          And the user is logged in as fullRegisteredUser1
          And the user is on page ShoppingCart
          And the user clicks onto the SparePartsTabButton-1
          And the shopping cart is empty

    @AUTOMATED @KSBP-4234 
    Scenario: Checkout Spare Parts with 2 different products (en-se)
        Given +++++++++++++++ START: user adds product SHAFT 32X 390 to shopping cart
            When the user clicks onto the SparePartsTabButton-1
            And the user enters value 47078081 into ProductAddInput
            And the user clicks onto the ProductAddSuggestItemProductSubmitButton-0
            And the user clicks onto the ProductAddSubmitButton
          Then the element ShoppingCartItem-0-0 is visible
        Given +++++++++++++++++ END: user adds product SHAFT 32X 390 to shopping cart
        
        Given +++++++++++++++ START: user adds product SPARE PARTS KIT ETN-WS25-01 to shopping cart
            When the user enters value 1513413 into ProductAddInput
            And the user clicks onto the ProductAddSuggestItemProductSubmitButton-0
            And the user clicks onto the ProductAddSubmitButton
          Then the element ShoppingCartItem-1-2 is visible
          	And there are 2 ShoppingCartItems displayed
        Given +++++++++++++++++ END: user adds product SPARE PARTS KIT ETN-WS25-01 to shopping cart
        
        Given +++++++++++++++ START: user enters reference name and proceeds to Shipping Terms
          When the user enters value test-reference into ReferenceNameInput
          	And the user clicks onto the SummaryCardProceedButton
        
          Then the element ProgressBarShippingTermsButton-1 is enabled
        Given +++++++++++++++++ END: user enters reference name and proceeds to Shipping Terms
        
        Given +++++++++++++++ START: user proceeds to Verification
          When the user clicks onto the SummaryCardProceedButton
          Then the element ProgressBarVerificationButton-2 is enabled
        Given +++++++++++++++++ END: user proceeds to Verification
        
        
        Given +++++++++++++++ START: user completes the order and proceeds to confirmation page
          When the user clicks onto the SummaryCardConsentCheckbox
          	And the user clicks onto the SummaryCardProceedButton
        
          Then the element ProgressBarShoppingCartButton-0 is disabled
            And the element ProgressBarShippingTermsButton-1 is disabled
            And the element ProgressBarVerificationButton-2 is disabled
            And the element ProgressBarConfirmationButton-3 is disabled
        Given +++++++++++++++++ END: user completes the order and proceeds to confirmation page


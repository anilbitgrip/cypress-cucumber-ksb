# language: en
@checkout    @stable    @demo-de-de    @newProducts    @matTest    
Feature: Checkout New Products - Quick Add - demo-de-de - permitted User

    Background: 
        Given the user is on site demo-de-de
          And the user is on page GlobalSearch
          And the user logs out
          And the user is logged in as fullRegisteredUser1

    @AUTOMATED @KSBP-4258 
    Scenario: failing adding a Product to cart from shopping cart for registerd full user
          And the user is on page ShoppingCart
          And the user clicks onto the NewProductsTabButton-0
         When the user enters value ghghdshsj into ShopCheckout ProductAddInput
         Then the element ProductAddSuggestItemProductName-0 is not visible
          And the element Tooltip is visible
          And the element ProductAddErrorMessage contains text value "Sorry, we could not find this material number. Please use the product search."


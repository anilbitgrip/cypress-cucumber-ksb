# language: en
@wts    @stable    @website    @frontend    @sprint-69    @alltypes-search    @global-search    
Feature: Alltypes Search - Spare parts material number - en-se - permitted user

    Background: 
        Given the user is on site en-se
          And the user is on page GlobalSearch
          And the user logs out
          And the user is logged in as fullRegisteredUser1
          And the user is on page GlobalSearch

    @AUTOMATED @KSBP-3565 @KSBP-3589 
    Scenario: Search global with 5 results on Spare parts material number
        When the user enters value 4204128 into GlobalProductSearchInput
          And the user clicks onto the GlobalProductSearchInputButton
         Then the element TabsButton-2 is visible
          And the element TabsButton-2 is enabled
          And the element TabsButtonBadge-2 is visible
         When the user clicks onto the TabsButton-2
         Then the element MaterialNumberListItem-4 is visible
         Then the element Price is visible
          And the element Price contains money amount
          And the element MaterialNumberListItem-5 is not visible
          And the element ShowMoreButton is not visible

    @AUTOMATED @KSBP-3565 @KSBP-3589 @KSBP-4277 
    Scenario: Search global with more than 5 Spare parts material number
        When the user enters value 4201507 into GlobalProductSearchInput
          And the user clicks onto the GlobalProductSearchInputButton
          And the user clicks onto the TabsButton-2
         Then the element MaterialNumberListItem-4 is visible
          And the element MaterialNumberListItem-5 is not visible
          And the element ShowMoreButton is visible
         When the user clicks onto the ShowMoreButton
         Then the element MaterialNumberListItem-5 is visible
          And the element ShowMoreButton is not visible

    @AUTOMATED 
    Scenario: Add DISC BOAX DN300 NICKELE .3J (42015078) spare part to cart from material number search result
        Given the user is on page ShoppingCart
          And the user clicks onto the SparePartsTabButton-1
          And the shopping cart is empty
        Given +++++++++++++++ START: user searches spare part and adds it to the cart
          And the user is on page GlobalSearch
         When the user enters value 4201507 into GlobalProductSearchInput
          And the user clicks onto the GlobalProductSearchInputButton
          And the user clicks onto the TabsButton-2
         Then the element MaterialNumberListItem-0 is visible
          When the user clicks onto the SparePartsMaterialNumberListAddToCartButton-0
        Given +++++++++++++++++ END: user searches spare part and adds it to the cart
        Given +++++++++++++++ START: user inspects the shopping cart content
          And the user is on page ShoppingCart
          And the user clicks onto the SparePartsTabButton-1
         Then the element ShoppingCartItem-0-0 is visible
         Then the element ProductNumber contains text value 42015078
        Given +++++++++++++++++ END: user inspects the shopping cart content

    @AUTOMATED @KSBP-4166 
    Scenario: Add to Cart and display Success Toast
            When the user enters value 4201507 into GlobalProductSearchInput
            And the user clicks onto the GlobalProductSearchInputButton
            And the user clicks onto the TabsButton-2
            And the user clicks onto the SparePartsMaterialNumberListAddToCartButton-0
            And the element SuccessToastNotification is visible


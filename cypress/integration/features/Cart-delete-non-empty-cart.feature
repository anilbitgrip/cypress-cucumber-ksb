# language: en
@website    @stable    @cart    
Feature: Cart - Delete non empty cart

    If there are any items in one of the carts there is a 'Delete Shopping cart' Link which is deleting the currently selected cart

    Background: A full registered user is logged in and has opened the shopping cart
        Given the user is on site en-se
          And the user is on page GlobalSearch
          And the user logs out
          And the user is logged in as fullRegisteredUser1
          And the user is on page ShoppingCart

    @AUTOMATED @KSBP-4230 
    Scenario: After adding a spare part the user can delete the spare part cart
        When the user clicks onto the SparePartsTabButton-1
          And the user enters value 1490524 into ProductAddInput
          And the user clicks onto the ProductAddSuggestItemProductSubmitButton-0
          And the user clicks onto the ProductAddSubmitButton
         Then the element ShoppingCartItem-0-0 is visible
         When the user clicks onto the DeleteCartButton
         Then the element DeleteCartOverlayApplyButton is visible
         When the user clicks onto the DeleteCartOverlayApplyButton
         Then the element EmptyShoppingCartBox is visible

    @AUTOMATED @KSBP-4230 
    Scenario: After adding a new product the user can delete the new product cart
          When the user enters value 29134765 into ProductAddInput
         And the user clicks onto the ProductAddSuggestItemProductSubmitButton-0
          And the user clicks onto the ProductAddSubmitButton
         Then the element ShoppingCartItem-0-0 is visible
        When the user clicks onto the DeleteCartButton
         Then the element DeleteCartOverlayApplyButton is visible
        When the user clicks onto the DeleteCartOverlayApplyButton
        Then the element EmptyShoppingCartBox is visible


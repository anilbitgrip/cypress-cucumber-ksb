# language: en
@todayFailedChromeOnly    @spareParts    @anilMyTest    @todayFailed    @website    @stable    
Feature: Spare Parts list - asynchronous price loading - demo-de-de

    Background: 
        Given the user is on site demo-de-de
          And the user is on page GlobalSearch
          And the user logs out

    # @AUTOMATED 
    Scenario: Olaf able to see spare parts material number price, fullRegisteredUser1
        And the user is logged in as fullRegisteredUser1
          And the user is on page GlobalSearch
         When the user clicks onto the GlobalProductSearchInputButton
          And the user clicks onto the TabsButton-2
         Then the element MaterialNumberListItem-0 Price is visible

    # @AUTOMATED 
    # Scenario: Olaf able to see spare parts material number price, fullRegisteredUser1
    #     And the user is logged in as fullRegisteredUser1
    #       And the user is on page GlobalSearch
    #      When the user clicks onto the GlobalProductSearchInputButton
    #       And the user clicks onto the TabsButton-2
    #       And the user looks onto content MaterialNumberListItem-0
    #      Then the element Price is visible

    # @AUTOMATED 
    # Scenario: Olaf able to see spare parts material number 02505144 price, fullRegisteredUser1
    #     And the user is logged in as fullRegisteredUser1
    #       And the user is on page GlobalSearch
    #      When the user enters value 4204128 into GlobalProductSearchInput
    #       And the user clicks onto the GlobalProductSearchInputButton
    #       And the user clicks onto the TabsButton-2
    #       And the user looks onto content MaterialNumberListItem-0
    #      Then the element Price is visible

    # @AUTOMATED 
    # Scenario: Anonymous user not able to see spare parts material number 000000000006216428 price
    #     And the user is on page GlobalSearch
    #      When the user enters value 000000000006216428 into GlobalProductSearchInput
    #       And the user clicks onto the GlobalProductSearchInputButton
    #       And the user clicks onto the TabsButton-3
    #       #And the user looks onto content SerialNumberListItem-0
    #       And the user clicks onto the SerialNumberListItem-0 Button
    #       #And the user looks onto content SparePartsSerialNumberMaterialListItem-0
    #      Then the element Price is not visible


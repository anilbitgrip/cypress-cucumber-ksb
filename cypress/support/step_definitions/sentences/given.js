import { Given } from 'cypress-cucumber-preprocessor/steps';
import { openSite, openPage, emptyShoppingCart } from '../given/index';

Given('the user is on site {word}', openSite);
Given('the user is on page {word}', openPage);
Given('the shopping cart is empty', emptyShoppingCart);
Given('+++++++++++++++ START: {}', async object => {});
Given('+++++++++++++++++ END: {}', async object => {});

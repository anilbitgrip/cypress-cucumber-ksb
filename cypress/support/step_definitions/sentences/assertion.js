import { Then } from 'cypress-cucumber-preprocessor/steps';
import {
  isVisible,
  isNotVisible,
  containsMoneyAmount
} from '../assertions/index';

Then(`the page has the path {word}`, pagePath => {
  cy.url().should('include', pagePath);
});

/**
 * contains text value STRING
 * TODO: explain
 */
Then(
  'the element {word} contains text value {word}',
  async (selector, text) => {
    cy.get(`[data-object-name="${selector}"]`).contains(text);
  }
);
Then(
  'the element {word} contains text value {string}',
  async (selector, text) => {
    cy.get(`[data-object-name="${selector}"]`).contains(text);
  }
);

Then('the content/element {word} is visible', isVisible);
Then('the content/element {word} {word} is visible', isVisible);
Then('the content/element {word} is not visible', isNotVisible);
Then('the content/element {word} {word} is not visible', isNotVisible);
/**
 * there are {number} {selector} displayed
 *
 * - only works when the user has focus onto content
 *
 * example:
 *
 * there are 5 CartListItems displayed
 *
 */
Then('there are {int} {word} displayed', async (number, selector) => {
  cy.get(`[data-object-counter="${selector}"]`).should('have.length', number);
});

/**
 * is enabled
 */
Then('the content/element {word} is enabled', async selector => {
  cy.get(`[data-element-name="${selector}"]`).should('be.enabled');
});
Then('the content/element {string} is enabled', async selector => {
  cy.get(`[data-element-name="${selector}"]`).should('be.enabled');
});
/**
 * is disabled
 */
Then('the content/element {word} is disabled', async selector => {
  cy.get(`[data-element-name="${selector}"]`).should('be.disabled');
});
Then('the content/element {string} is disabled', async selector => {
  cy.get(`[data-element-name="${selector}"]`).should('be.disabled');
});

/**
 * contains money amount
 */
Then('the content/element {word} contains money amount', containsMoneyAmount);
Then('the content/element {string} contains money amount', containsMoneyAmount);

/**
 * is not visible
 */
// Then('the content/element {word} is not visible', async element => {
//   const { truthy, message } = await isNotVisible(element);
//   expect(truthy).to.equal(true, message);
// });
// Then('the content/element {string} is not visible', async element => {
//   const { truthy, message } = await isNotVisible(element);
//   expect(truthy).to.equal(true, message);
// });

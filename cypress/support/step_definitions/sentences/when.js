import { When } from 'cypress-cucumber-preprocessor/steps';
import {
  openPagePath,
  clicksOnto,
  logOut,
  loggedInAs,
  enterValue
} from '../actions';

When('the user open page path {word}', openPagePath);
When('the user clicks onto the {word}', clicksOnto);
When('the user clicks onto the {word} {word}', clicksOnto);
When('the user logs out', logOut);
When('the user is logged in as {word}', loggedInAs);

When('the user enters value {word} into {word}', enterValue);
When('the user enters value {word} into {word} {word}', enterValue);
When('the user enters value {string} into {word}', enterValue);
When('the user enters value {word} into {string}', enterValue);
When('the user enters value {string} into {string}', enterValue);

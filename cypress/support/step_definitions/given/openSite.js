let site = false;

const openSite = path => {
  site = path;
  cy.viewport(Cypress.env('SCREEN_WIDTH'), Cypress.env('SCREEN_HEIGHT'));
  cy.visit(path);
};

export const getSite = () => site;

export default openSite;

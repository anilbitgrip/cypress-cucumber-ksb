import { getSite } from './index';
import { actionsConfig } from '../_helper/config/index';
import getSpecialPage from '../_helper/getSpecialPage';

const openPage = async page => {
  const site = getSite();

  expect(!!site).to.equal(
    true,
    'Expected Site to be set before opening a page'
  );

  const { url } = await getSpecialPage(site, page);

  // look onto all
  // await lookOnto(false);

  await cy.pause(actionsConfig.timing.pageLoad);

  cy.setCookie(
    actionsConfig.cookieConstent.name,
    actionsConfig.cookieConstent.value
  );

  await cy.visit(url);
  await cy.pause(actionsConfig.timing.pageLoad);
};

export default openPage;

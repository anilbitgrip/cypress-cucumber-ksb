const emptyShoppingCart = async () => {
  await cy.get('body').then(async $body => {
    if ($body.find('[data-object-name="DeleteCartButton"]').length) {
      cy.get('[data-object-name="DeleteCartButton"]').click();
      cy.get('[data-object-name="DeleteCartOverlayApplyButton"]').click();
    }
  });
};

export default emptyShoppingCart;

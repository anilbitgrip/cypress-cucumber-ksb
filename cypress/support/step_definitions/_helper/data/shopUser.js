export const shopUser = {
  fullRegisteredUser1: {
    username: 'named02_1',
    password: 'password1!'
  },
  fullRegisteredUser2: {
    username: 'named02_2',
    password: 'password1!'
  },
  fullRegisteredUser3: {
    username: 'named01_3',
    password: 'password1!'
  },
  fullRegisteredUser4: {
    username: 'named02_1',
    password: 'password1!'
  },
  fullRegisteredUser5: {
    username: 'named02_2',
    password: 'password1!'
  },
  fullRegisteredUser6: {
    username: 'ksbtestcustomer',
    password: 'password1!'
  },
  aribaUser: {
    username: 'ksbtestcustomerariba',
    password: 'password1!'
  },
  ociUser: {
    username: 'ksbocitestcustomer',
    password: 'password1!'
  },
  changePasswordUser: {
    username: 'changePasswordUserEmail',
    password: 'password1@'
  },
  newlyRegisteredUser: {
    username: 'newlyRegisteredUserEmail',
    password: 'password1!'
  }
};

import axios from 'axios';

const assertThatConfig = Cypress.env('assertThat');

const axiosInstance = axios.create();

const BASE_URL = Cypress.env('BASE_URL');
const PATH_TO_NODE_API = Cypress.env('PATH_TO_NODE_API');

const apiCache = {
  getSpecialPages: {}
};

const getSpecialPages = async site => {
  if (apiCache.getSpecialPages[site]) {
    return apiCache.getSpecialPages[site];
  }

  return axiosInstance
    .get(`${BASE_URL}${PATH_TO_NODE_API}test-special-pages/${site}`)
    .then(response => {
      Object.keys(response.data).forEach(key => {
        if (response.data[key].path) {
          response.data[key].url =
            BASE_URL + response.data[key].path.substring(1);
        }
      });
      apiCache.getSpecialPages[site] = response.data;
      return response.data;
    })
    .catch(() => 'error fetching special pages');
};

const getSpecialPagesSync = site => {
  if (apiCache.getSpecialPages[site]) {
    return apiCache.getSpecialPages[site];
  }
  return {};
};

export const NodeAppApi = {
  getSpecialPages,
  getSpecialPagesSync
};

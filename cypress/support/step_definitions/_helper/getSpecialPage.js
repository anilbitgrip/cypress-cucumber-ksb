import {
  NodeAppApi
} from '../_helper/nodeApp';

export const getSpecialPage = async (site, page) => {
  const specialPages = await NodeAppApi.getSpecialPages(site);

  console.log('///////////////// pecialPages ---->', specialPages);
  const specialPage = specialPages[page];

  expect(specialPage).to.not.equal(
    undefined,
    `There is no special page '${page}' configured.`
  );

  if (specialPage === undefined) return false;

  return specialPage;
};

export default getSpecialPage;
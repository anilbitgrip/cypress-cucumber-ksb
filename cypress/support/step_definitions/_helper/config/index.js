export const actionsConfig = {
  timing: {
    pageLoad: 2000,
    waitUntil: 10000,
    waitScroll: 2000,
    serverResponse: 3000
  },
  cookieConstent: {
    name: 'accepts-cookies',
    value: 'a'
  }
};

import { actionsConfig } from './config';
import 'cypress-wait-until';

export const CHECK_TYPE = {
  whole: 'whole',
  part: 'part',
  pattern: 'pattern'
};

export const CHECK_TYPE_ERROR_TEXT = {
  whole: 'be',
  part: 'contain',
  pattern: 'match'
};
/**
 * wait default server response time until text changes
 * @param  {String}   text
 * @param  {String}   selector DOM selector
 * @param  {String}   checkType
 */
const browserWaitUntilTextValueChange = async (
  text,
  selector,
  checkType = CHECK_TYPE.whole
) => {
  const timoutSeconds = actionsConfig.timing.serverResponse / 1000;
  const timeoutMsg = `expected ${selector} text to ${CHECK_TYPE_ERROR_TEXT[checkType]} ${text} after ${timoutSeconds}s`;
  let checkFunc;

  if (checkType === CHECK_TYPE.part) {
    checkFunc = testValue => testValue && testValue.contains(text);
  } else if (checkType === CHECK_TYPE.pattern) {
    const regex = new RegExp(text);
    checkFunc = testValue => regex.test(testValue);
  } else {
    checkFunc = testValue => testValue === text;
  }

  // with all the available options
  return cy.waitUntil(
    () =>
      cy
        .get(`[data-element-name="${selector}"]`)
        .then($el => checkFunc($el.text())),
    {
      errorMsg: `${timeoutMsg}`, // overrides the default error message
      timeout: actionsConfig.timing.serverResponse, // waits up to 2000 ms, default to 5000
      interval: 500 // performs the check every 500 ms, default to 200
    }
  );
};

export default browserWaitUntilTextValueChange;

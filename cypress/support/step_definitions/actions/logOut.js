const logOut = async () => {
  await cy.get('[data-element-name="MetaNavigationProfileButton"]').click();
  const $logOutButton = await cy
    .get('[data-element-name="MetaNavigationProfileButton"]')
    .then($el => $el.find('[data-element-name="LogOut"]').length > 0);

  const $loginButton = await cy
    .get('[data-element-name="LoginLink"]')
    .then($el => $el.find('[data-element-name="LoginLink"]').length > 0);

  if (!$loginButton && $logOutButton) {
    await cy.get('[data-element-name="logOut"]').click();
  }
};

export default logOut;

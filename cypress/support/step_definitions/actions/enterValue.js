/**
 * @method enterValue
 * @param {*} value input value to be entered
 * @param {*} selector1  is parent selector if selector2 is also passed, else it's current selector
 * @param {*} selector2  is current selector
 */
const enterValue = async (value, selector1, selector2 = null) => {
  const parentSelector = selector2 ? `[data-object-name="${selector1}"]` : '';
  const currentSelector = selector2
    ? `[data-element-name="${selector2}"]`
    : `[data-element-name="${selector1}"]`;

  if (currentSelector.includes('Textarea')) {
    await cy.get(`${parentSelector} ${currentSelector}`).type(value);
  } else {
    await cy.get(`${parentSelector} ${currentSelector} input`).type(value);
  }
};

export default enterValue;

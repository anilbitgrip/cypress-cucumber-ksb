import { getSite } from '../given/index';
const openPagePath = path => {
  const site = getSite();
  cy.visit(site + '/' + path);
};

export default openPagePath;

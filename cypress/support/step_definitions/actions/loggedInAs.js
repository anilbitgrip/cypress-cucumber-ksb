/**
 * Open the given Page
 */
import { shopUser } from '../_helper/data/shopUser';
import openPage from '../given/openPage';
import { getSite } from '../given/openSite';

const loggedInAs = async user => {
  const site = getSite();

  expect(!!site).to.equal(true, 'Expected Site to be set before logging in');

  const { username, password } = shopUser[user];

  // check if user is known
  expect(password).to.not.equal(
    undefined,
    `user '${username}' not found in test environment`
  );

  await openPage('Login');

  cy.get('[data-element-name="Login"]').within(async $form => {
    await cy.get('[data-element-name="UsernameInput"] input').type(username);
    await cy.get('[data-element-name="PasswordInput"] input').type(password);
    await cy.get('[data-element-name="SubmitButton"]').click();
    // cy.get('[data-element-name="Login"] form').submit();
  });
};

export default loggedInAs;

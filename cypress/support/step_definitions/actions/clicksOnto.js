const clicksOnto = async (selector1, selector2) => {
  let customeSelector = `[data-element-name="${selector1}"]`;

  if (selector2) {
    customeSelector = `[data-element-name="${selector1}"] [data-element-name="${selector2}"]`;
  }

  cy.get(customeSelector).click({
    force: true
  });
};

export default clicksOnto;
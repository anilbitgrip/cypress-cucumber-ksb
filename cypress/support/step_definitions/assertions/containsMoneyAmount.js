import browserWaitUntilTextValueChange, {
  CHECK_TYPE
} from '../_helper/browserWaitUntilTextValueChange';

/**
 * Example for pattern
 * EUR 0.00
 * EUR 0,00
 * EUR 1.000,00
 * 00.00 EUR
 * 00,00 EUR
 * 00,00 kr
 * 00,00 €
 * 00,00 $
 * $ 00,00
 * € 00,00
 * @param {*} selector
 */
const containsMoneyAmount = async selector => {
  const pattern = '([A-Z€$]{1,3}\\s[0-9,.]{4,}|[0-9,.]{4,}\\s[A-Za-z€$]{1,3})';
  await browserWaitUntilTextValueChange(pattern, selector, CHECK_TYPE.pattern);

  await cy.get(`[data-element-name="${selector}"]`).then($el => {
    const text = $el.text();
    let truthy = false;
    const regExp = new RegExp(pattern);
    if (regExp.test(text)) {
      truthy = true;
    }
    expect(truthy).to.equal(
      true,
      `Expected object "${selector}" to contain a money amount, but it contains ${text}`
    );
  });
};

export default containsMoneyAmount;

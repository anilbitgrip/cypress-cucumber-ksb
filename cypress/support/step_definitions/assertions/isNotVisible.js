const isNotVisible = async selector =>
  cy.get('body').then($el => {
    const truthy = $el.find(`[data-element-name="${selector}"]`).length === 0;
    expect(truthy).to.equal(
      true,
      `Expected object "${selector}" not to be visible`
    );
  });

export default isNotVisible;
